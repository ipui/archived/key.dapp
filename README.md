# ipui key dapp

single purpose dapp to manage your ipfs node keys

this web dapp is developed to work with mutable file system [msf](https://github.com/ipfs/interface-js-ipfs-core/blob/master/SPEC/FILES.md#the-files-api-aka-mfs-the-mutable-file-system) of [ipfs](https://ipfs.io).

**this is not an implementation of an entire ipfs node**, this project only use the `api` that the _daemon_ binds over the port `5001` (by default). [go](https://github.com/ipfs/go-ipfs) or [javascript](https://github.com/ipfs/js-ipfs) implementation is by your own.

## ip[fn]s links

* [/ipns/12D3KooWEZYHR92C1pjioxBf3nvW1hsdiAuZpJNXUPwJoMyFtp2h](https://gateway.ipfs.io/ipns/12D3KooWEZYHR92C1pjioxBf3nvW1hsdiAuZpJNXUPwJoMyFtp2h).
* [/ipns/key.dapp.rabrux.space](https://gateway.ipfs.io/ipns/key.dapp.rabrux.space).

comming soon.

## description

then this micro dapp only executes the little task of manage your ipfs keychain.

## technologies

* [reactjs](https://reactjs.org)
* [ipfs](https://ipfs.io)

## depends on

* [@ipui/ipfs](https://gitlab.com/ipui/core/context/ipfs)
* [@ipui/path](https://gitlab.com/ipui/core/context/path)
* [@ipui/skin](https://gitlab.com/ipui/core/context/skin)

## dev

> This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## contribute

1. test.
2. pin.
3. report issue.
4. review the code.
5. resolve an issue.

## last update
sep 2, 2019
