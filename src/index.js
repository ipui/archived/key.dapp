import React from 'react';
import ReactDOM from 'react-dom';

import '@ipui/skin/index.css'
import '@ipui/skin/theme/moon.css'
import Main from './component/_main/Main'

ReactDOM.render(<Main />, document.getElementById('root'));
