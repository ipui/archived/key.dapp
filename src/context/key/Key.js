import React, { createContext, Component } from 'react'
import PropTypes from 'prop-types'

import { withPath } from '@ipui/path'
import { withIpfs } from '@ipui/ipfs'

const KeyContext = createContext()

class Key extends Component {

  state = {
    list: null
  }

  static propTypes = {
    children: PropTypes.node.isRequired
  }

  constructor( props ) {
    super( props )
    this.updateKeys = this.updateKeys.bind( this )
  }

  componentDidMount() {
    this.updateKeys()
  }

  updateKeys() {
    const { list } = this.props.withIpfs.node.key
    list( ( err, keys ) => {
      if ( err )
        console.error( 'list', err )

      this.setState( {
        ...this.state,
        list: keys
      } )
    } )
  }

  render() {
    const { children } = this.props
    const { updateKeys } = this
    const { gen, rename, rm } = this.props.withIpfs.node.key
    const { list } = this.state

    if ( !list )
      return ( <></> )

    return (
      <KeyContext.Provider value={ {
        list,
        updateKeys,

        gen,
        rename,
        rm
      } } >
        { children }
      </KeyContext.Provider>
    )
  }
}

const withKey = ( ComponentAlias ) => {

  return props => (
    <KeyContext.Consumer>
      { context => {
        return <ComponentAlias { ...props } withKey={ context } />
      } }
    </KeyContext.Consumer>
  )

}

export default withPath( withIpfs( Key ) )

export {
  KeyContext,
  withKey
}
