import React from 'react'
import PropTypes from 'prop-types'

import {
  FiHeart,
  FiGitlab
} from 'react-icons/fi'

import {
  IoIosBug
} from 'react-icons/io'

import './IpfsPlaceholder.sass'

const IpfsPlaceholder = props => {

  const { generator } = props

  return (
    <section className="ipfs-placeholder">
      <p>
      an <a href="https://ipfs.io" target="_blank">ipfs</a> node is required to use this dapp.
      </p>
      <p>
        made with <FiHeart />
      </p>
      <ul>
        <li>
          <a href={ generator } target="_blank">
            <FiGitlab /> source code
          </a>
        </li>
        <li>
          <a href={ generator + '/issues' } target="_blank">
            <IoIosBug /> contribute
          </a>
        </li>
      </ul>
    </section>
  )
}

IpfsPlaceholder.propTypes = {
  generator: PropTypes.string.isRequired
}

export default IpfsPlaceholder
