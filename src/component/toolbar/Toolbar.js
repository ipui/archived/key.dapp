import React, { useState } from 'react'

import { FiPlus } from 'react-icons/fi'

import KeyForm from '../key-form/KeyForm'

import './Toolbar.sass'

const Toolbar = props => {
  const [ isKeyFormOpen, setIsKeyFormOpen ] = useState( false )

  function toggle() {
    setIsKeyFormOpen( !isKeyFormOpen )
  }

  return (
    <>

      { isKeyFormOpen ? (

        <KeyForm toggle={ toggle } />

      ) : (

        <footer className="toolbar">
          <button onClick={ toggle }>
            <FiPlus />
          </button>
        </footer>

      ) }

    </>
  )
}

export default Toolbar
