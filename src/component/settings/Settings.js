import React, { useState } from 'react'

import {
  FiSettings,
  FiX
} from 'react-icons/fi'

import IpfsConnect from '../../component/ipfs-connect/IpfsConnect'

const Settings = props => {

  const [ isSettingsOpen, setIsSettingsOpen ] = useState( false )

  function toggle() {
    setIsSettingsOpen( !isSettingsOpen )
  }

  return (
    <>

      <header>

        <h1>
          keychain
        </h1>

        <button onClick={ toggle }>

          { isSettingsOpen ? (
            <FiX />
          ) : (
            <FiSettings />
          ) }

        </button>

      </header>

      { isSettingsOpen ? (
        <header>
          <IpfsConnect />
        </header>
      ) : null }

    </>
  )
}

export default Settings
