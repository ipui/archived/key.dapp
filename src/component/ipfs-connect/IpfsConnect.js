import React, { useState } from 'react'
import multiaddr from 'multiaddr'

import RouteTools from '@ipui/path/dist/lib/RouteTools'

import {
  FiArrowRight
} from 'react-icons/fi'

const Connect = props => {

  const [ address, setAddress ] = useState( '' )

  function updateAddress( event ) {
    setAddress( event.target.value )
  }

  function isAddressValid() {
    try {
      const {
        family,
        host,
        transport,
        port
      } = multiaddr( address ).toOptions()

      if ( family && host && transport && port )
        return true

      return false
    } catch ( e ) {
      // TODO implement error handler as toast
      console.error( e )
      return false
    }
  }

  function connect() {
    if ( isAddressValid() )
      return RouteTools._go( RouteTools.applyHash( address ), { reload: true } )
  }

  function onEnter( event ) {
    if ( event.keyCode === 13 )
      connect()
  }

  return (
    <>

      <input
        placeholder="/ip4/127.0.0.1/tcp/5001"
        onChange={ updateAddress }
        onKeyDown={ onEnter }
        size="1"
      />

      <button onClick={ connect }>
        <FiArrowRight />
      </button>

    </>
  )

}

export default Connect

