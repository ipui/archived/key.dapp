import React, { useState } from 'react'
import PropTypes from 'prop-types'

import { withKey } from '../../context/key/Key'

import {
  FiX,
  FiCheck
} from 'react-icons/fi'

const KeyListItemRename = props => {

  const [ newName, setNewName ] = useState( '' )

  function resetNameForm() {
    const { toggle } = props
    toggle()
  }

  function changeKeyName() {
    const { rename, updateKeys } = props.withKey
    const { oldName } = props
    rename( oldName, newName, ( err, key ) => {
      if ( err )
        return console.error( 'rename', err )

      resetNameForm()
      updateKeys()
    } )
  }

  const placeholder = props.oldName

  return (
    <>
      <input
        placeholder={ placeholder || 'new name' }
        value={ newName }
        onChange={ e => setNewName( e.target.value ) }
        onKeyDown={ e => {
          if ( e.keyCode === 13 )
            changeKeyName()
        } }
        autoFocus
      />
      <button onClick={ changeKeyName }>
        <FiCheck />
      </button>
      <button onClick={ resetNameForm }>
        <FiX />
      </button>
    </>
  )
}

KeyListItemRename.propTypes = {
  oldName: PropTypes.string.isRequired,
  toggle: PropTypes.func.isRequired
}

export default withKey( KeyListItemRename )
