import React, { useState } from 'react'
import PropTypes from 'prop-types'

import {
  FiCheck,
  FiArrowLeft,
  FiX
} from 'react-icons/fi'

const KeyFormType = props => {

  function handleNext() {
    const { next } = props
    next()
  }

  function handleCancel() {
    const { resetModel } = props
    resetModel()
  }

  function handleChangeType( event ) {
    const { model, setModel } = props
    const { value } = event.target

    let length = model.length || '2048'
    if ( value === 'ed25519' )
      length = ''

    setModel( {
      ...model,
      type: value,
      length: length
    } )
  }

  function handleChangeLength( event ) {
    const { model, setModel } = props
    setModel( {
      ...model,
      length: event.target.value
    } )
  }

  const { type, length } = props.model

  return (
    <section className="key-form">
      <header>
        <select
          value={ type }
          onChange={ handleChangeType }
        >
          <option disabled>type</option>
          <option>rsa</option>
          <option>ed25519</option>
        </select>

        { type === 'rsa' ? (
          <select
            value={ length }
            onChange={ handleChangeLength }
          >
            <option disabled>length</option>
            <option>2048</option>
            <option>4096</option>
          </select>
        ) : null }

        <button onClick={ handleNext }>
          <FiCheck />
        </button>

        <button onClick={ handleCancel }>
          <FiX />
        </button>
      </header>
    </section>
  )
}

KeyFormType.propTypes = {
  placeholder: PropTypes.string,
  model: PropTypes.exact( {
    name: PropTypes.string,
    type: PropTypes.oneOf( [
      'rsa',
      'ed25519'
    ] ),
    length: PropTypes.oneOf( [
      '2048',
      '4096',
      ''
    ] )
  } ),
  setModel: PropTypes.func.isRequired,
  resetModel: PropTypes.func.isRequired,
  next: PropTypes.func.isRequired
}

export default KeyFormType
