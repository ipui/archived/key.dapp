import React, { useState } from 'react'
import PropTypes from 'prop-types'

import {
  FiArrowRight,
  FiX
} from 'react-icons/fi'

const KeyFormName = props => {

  function handleNext() {
    const { next } = props
    const { name } = props.model

    if ( name.length > 0 )
      next()
  }

  function handleCancel() {
    const { resetModel } = props
    resetModel()
  }

  const { placeholder, setModel } = props
  const { name } = props.model

  return (
    <section className="key-form">
      <header>
        <input
          value={ name }
          placeholder={ placeholder || 'key name' }
          autoFocus
          size="1"
          onChange={ e => setModel( { ...props.model, name: e.target.value } ) }
          onKeyDown={ e => {
            if ( e.keyCode === 13 )
              handleNext()
          } }
        />

        <button onClick={ handleNext }>
          <FiArrowRight />
        </button>

        <button onClick={ handleCancel }>
          <FiX />
        </button>
      </header>
    </section>
  )
}

KeyFormName.propTypes = {
  placeholder: PropTypes.string,
  model: PropTypes.exact( {
    name: PropTypes.string,
    type: PropTypes.oneOf( [
      'rsa',
      'ed25519'
    ] ),
    length: PropTypes.oneOf( [
      '2048',
      '4096',
      ''
    ] )
  } ),
  setModel: PropTypes.func.isRequired,
  resetModel: PropTypes.func.isRequired,
  next: PropTypes.func.isRequired
}

export default KeyFormName
