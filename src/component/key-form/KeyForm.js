import React, { useState } from 'react'
import PropTypes from 'prop-types'

import { withKey } from '../../context/key/Key'

import KeyFormName from '../key-form-name/KeyFormName'
import KeyFormType from '../key-form-type/KeyFormType'

const KeyForm = props => {
  const [ model, setModel ] = useState( {
    name: '',
    type: 'rsa',
    length: '2048'
  } )

  const [ step, setStep ] = useState( 0 )

  function back() {
    setStep( step - 1 )
  }

  function next() {
    setStep( step + 1 )
  }

  function resetModel() {
    const { toggle } = props
    setModel( {
      name: '',
      type: 'rsa',
      length: '2048'
    } )
    toggle()
  }

  function keyGen() {
    const { gen, updateKeys } = props.withKey
    const { name, type, length } = model
    gen( name, {
      type,
      length
    } )
      .then( key => {
        updateKeys()
        resetModel()
      } )
      .catch( err => {
        console.error( 'keygen', err )
      } )
  }

  return (
    <section className="key-form">
      <>

        { step === 0 ? (
          <KeyFormName
            model={ model }
            setModel={ setModel }
            resetModel={ resetModel }
            next={ next }
          />
        ) : null }

        { step === 1 ? (
          <KeyFormType
            model={ model }
            setModel={ setModel }
            resetModel={ resetModel }
            next={ keyGen }
          />
        ) : null }

      </>
    </section>
  )
}

KeyForm.propTypes = {
  toggle: PropTypes.func.isRequired
}

export default withKey( KeyForm )
