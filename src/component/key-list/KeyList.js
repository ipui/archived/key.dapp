import React from 'react'

import { withKey } from '../../context/key/Key'

import KeyListItem from '../key-list-item/KeyListItem'

const KeyList = props => {
  const { list } = props.withKey

  const itemList = list.map( el => {
    return (
      <li key={ el.id }>
        <KeyListItem item={ el } />
      </li>
    )
  } )

  return (
    <main>
      <ul>
        { itemList }
      </ul>
    </main>
  )
}

export default withKey( KeyList )
