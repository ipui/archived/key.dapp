import React, { useState } from 'react'
import PropTypes from 'prop-types'

import {
  FiEdit2,
  FiTrash
} from 'react-icons/fi'

import { withKey } from '../../context/key/Key'

import KeyListItemRename from '../key-list-item-rename/KeyListItemRename'
import DevilsFive from '../devils-five/DevilsFive'

const KeyListItem = props => {
  const [ showName, setShowName ] = useState( true )

  const [ isRemoveOpen, setIsRemoveOpen ] = useState( false )
  function toggleRemove() {
    setIsRemoveOpen( !isRemoveOpen )
  }

  const [ isNameForm, setIsNameForm ] = useState( false )
  function toggleNameForm() {
    setIsNameForm( !isNameForm )
  }

  function removeKey() {
    const { rm, updateKeys } = props.withKey
    const { name } = props.item
    rm( name, ( err, key ) => {
      if ( err )
        return console.error( 'rm', err )

      updateKeys()
    } )
  }

  const { name, id } = props.item

  return (
    <>

      { isNameForm ? (
        <KeyListItemRename
          toggle={ toggleNameForm }
          oldName={ name }
        />
      ) : (
        <>
          <label onClick={ e => setShowName( !showName ) }>
            { showName ? name : id }
          </label>

          { isRemoveOpen ? (
            <DevilsFive
              label="recover"
              action={ removeKey }
              toggle={ toggleRemove }
            />
          ) : name !== 'self' ? (
            <>
              <button onClick={ toggleNameForm }>
                <FiEdit2 />
              </button>
              <button onClick={ toggleRemove }>
                <FiTrash />
              </button>
            </>
          ) : null }

        </>
      ) }

    </>
  )
}

KeyListItem.propTypes = {
  item: PropTypes.object.isRequired
}

export default withKey( KeyListItem )
