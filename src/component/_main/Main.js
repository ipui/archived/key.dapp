import React from 'react'

import Path from '@ipui/path'
import Ipfs from '@ipui/ipfs'
import Key from '../../context/key/Key'

import Settings from '../settings/Settings'
import Toolbar from '../toolbar/Toolbar'
import KeyList from '../key-list/KeyList'

import IpfsPlaceholder from '../ipfs-placeholder/IpfsPlaceholder'

const Main = props => {

  return (
    <Path>
      <Ipfs placeholder={ (
        <>
          <IpfsPlaceholder generator="https://gitlab.com/ipui/dapp/key.dapp" />
        </>
      ) }>
        <Key>

          <Settings />

          <KeyList />

          <Toolbar />

        </Key>
      </Ipfs>
    </Path>
  )
}

export default Main
